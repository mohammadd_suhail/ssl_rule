import os
import json
import glob
from os import listdir
from os.path import isfile, join

Z_CLOUDS = {"10.66.64.214": "zscalerone.net",
            "10.66.64.164": "zscalertwo.net",
            "10.66.64.184": "zscalerthree.net",
            "10.66.64.120": "zscaler.net",
            "10.66.64.154": "zscloud.net"}


def get_Json(dir_path):
    dir_path = dir_path
    # files = [f for f in listdir(dir_path) if isfile(join(dir_path, f))]
    file_paths = glob.glob(dir_path + r"\*.json")
    err_msg = []
    cloud_json_dt = {}
    if file_paths:
        for cloud_ip, cloud_name in Z_CLOUDS.items():
            json_dt = {}
            for file_path in file_paths:
                if file_path.find(cloud_ip) > 0:
                    file_name = file_path[file_path.rfind(os.sep) + 1:]
                    with open(file_path) as f:
                        json_data = f.read()
                        json_object = ""
                        try:
                            json_object = json.loads(json_data)
                            del json_data
                        except ValueError:
                            err_msg.append("Unable to load JSON data for file %s" % file_name)
                        else:
                            for dt in json_object:
                                domain = dt["domain"]
                                endpoint = dt["endpointsSnapshot"]
                                modified_endpoint = get_data(endpoint)
                                json_dt[domain] = modified_endpoint
                        del json_object
                    cloud_json_dt[cloud_name] = json_dt
        del json_dt
    else:
        err_msg.append("No JSON file found in the specified directory.")
    return cloud_json_dt, err_msg


def get_URL_category_dict(url_category_lst):
    url_category_dt = {}
    for record in url_category_lst:
        id = record.get('id')
        if id is not None and bool(id.lower().startswith('custom_')):
            url_category_dt[id] = record.get('configuredName', id)
    return url_category_dt


def get_data(endpoint_dt):
    field_table_dt = {}
    url_category_lst = endpoint_dt.get('urlCategories')
    url_category_dt = {}
    if url_category_lst:
        url_category_lst = url_category_lst[:]
        url_category_dt = get_URL_category_dict(url_category_lst)
    for endpoint, value in endpoint_dt.items():
        if type(value) is dict:
            endpoint_data_dt = {}
            for key, data in value.items():
                field_dt = {}
                data_count = 1
                if type(data) is list:
                    data_lst = []
                    data_count = len(data)
                    if key.lower().endswith("categories"):
                        for category in data:
                            data_lst.append(url_category_dt.get(category, category))
                        data = data_lst
                    endpoint_data_dt[key] = {data_count: ",".join(data)}
                else:
                    endpoint_data_dt[key] = {data_count: str(data)}
            field_table_dt[endpoint] = {'field': endpoint_data_dt}
        else:
            num_of_recs = len(value)
            endpoint_data_dt = {}
            if num_of_recs > 0:
                first_item = value[0]
                if type(first_item) is dict:
                    header = {}
                    for value_dt in value:
                        header.update(value_dt)
                    header = list(header.keys())
                    records = []
                    for value_dt in value:
                        table_data = {}
                        for heading in header:
                            data = value_dt.get(heading)
                            if type(data) is list:
                                try:
                                    data = ",".join(data)
                                except TypeError:
                                    data = str(data)
                            else:
                                data = str(data)
                            table_data[heading] = data
                        records.append(table_data)
                    field_table_dt[endpoint] = {'table': records, 'headers': header}
                else:
                    field_table_dt[endpoint] = {'data': ",".join(value)}
            else:
                field_table_dt[endpoint] = {'blank': ""}

    return field_table_dt
