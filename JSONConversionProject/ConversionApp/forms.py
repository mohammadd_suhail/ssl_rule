from django import forms


class JSONUploadFileForm(forms.Form):
    json_file_dir_path = forms.CharField(
        error_messages={'required': 'Please enter valid directory path.'})
