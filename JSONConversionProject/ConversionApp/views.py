from django.contrib import messages
from django.shortcuts import render, redirect
from ConversionApp.forms import JSONUploadFileForm
from ConversionApp.json_data_utils import get_Json


# Create your views here.


def index(request):
    json_upload_file_form = JSONUploadFileForm()
    if request.method == 'POST':
        json_upload_file_form = JSONUploadFileForm(request.POST)
        if json_upload_file_form.is_valid():
            dir_path = json_upload_file_form.cleaned_data
            dir_path = dir_path.get('json_file_dir_path').strip()
            json_data, err_msg = get_Json(dir_path)
            if len(err_msg) == 0:
                request.session['json_object'] = json_data
                return redirect('/client_data/')
            else:
                err = "\n".join(err_msg)
                json_upload_file_form.add_error("json_file_dir_path", err)
        else:
            json_upload_file_form.add_error("json_file_dir_path", "Invalid form submission")
    return render(request, 'json_conversion/index.html', {"json_form": json_upload_file_form})


def client_data(request):

    if request.method == "GET":
        selected_cloud_name = request.GET.get('cloud')
        selected_domain_name = request.GET.get('tab')
        if selected_cloud_name not in ["", None] and selected_domain_name in ["", None]:
            endpoints_lst = []
            customized_json_data = {}
            return render(request, "json_conversion/client_data.html", {"endpoints": endpoints_lst,
                                                                        "selected_cloud_name": selected_cloud_name,
                                                                        "selected_domain_name": selected_domain_name,
                                                                        "customized_json_data": customized_json_data,
                                                                        })
        else:
            if selected_cloud_name not in ["", None] and selected_domain_name not in ["", None]:
                try:
                    customized_json_data = request.session.get('json_object')[selected_cloud_name][selected_domain_name]
                    endpoints_lst = list(customized_json_data.keys())
                except:
                    return redirect('/client_data/')
                return render(request, "json_conversion/client_data.html", {"endpoints": endpoints_lst,
                                                                            "selected_domain_name": selected_domain_name,
                                                                            "customized_json_data": customized_json_data,
                                                                            })

            else:
                try:
                    customized_json_data = request.session.get('json_object')
                    cloud_names_lst = list(customized_json_data.keys())
                except:
                    return redirect('/')
                else:
                    selected_cloud_name = cloud_names_lst[0]
                    selected_domain_name = ""
                return render(request, "json_conversion/client_data.html", {"cloud_names": cloud_names_lst,
                                                                            "selected_cloud_name": selected_cloud_name,
                                                                            "selected_domain_name": selected_domain_name,
                                                                            "customized_json_data": customized_json_data})

    if request.method == "POST":
        del request.session['json_object']
    return redirect('/')
