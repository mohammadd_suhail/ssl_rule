from django import template
register = template.Library()


@register.simple_tag
def get(dt, key):
    return dt.get(key, '')


@register.simple_tag
def get_keys_list(dt, key):
    return list(dt.get(key, ''))


@register.simple_tag
def get_sub_key_list(dt, key, sub_key):
    lst_of_sub_keys = []
    main_key = dt.get(key, '')
    if main_key not in ["", None]:
        lst_of_sub_keys = list(main_key.get(sub_key, ""))
    return lst_of_sub_keys


@register.simple_tag
def get_leaf_dict(dt, cloud_key, domain_key, endpoint_key):
    leaf_dt = {}
    endpoint_dt = {}
    domain_dt = dt.get(cloud_key, '')
    if domain_dt not in ["", None]:
        endpoint_dt = domain_dt.get(domain_key, "")
    if endpoint_dt not in ["", None]:
        leaf_dt = endpoint_dt.get(endpoint_key, "")
    return leaf_dt

@register.simple_tag
def get_keys(dt):
    return list(dt.keys())


@register.simple_tag
def get_values(dt):
    return list(dt.values())


