from django.urls import path
from ConversionApp import views

urlpatterns = [
    path('', views.index),
    path('client_data/', views.client_data)
]