
function fetch_domains(cloud_name, domain_name)
{
    var divs = document.getElementById("vertical_menu_" + cloud_name).getElementsByTagName('div')
    var flag = false;
    for(var i=0; i < divs.length; i++)
    {
        if(divs[i].id == domain_name)
        {
            flag = true;
            break;
        }
    }
    return flag;
}

$(document).ready(function (){
    $('.search-button').click(function (){
        var cloud_names_dropdown = document.getElementById('search_cloud_name');
        var domain_name = document.getElementById('search_field').value;
        selected_dropdown_cloud_name = "";
        if (cloud_names_dropdown)
        {
            for(var i=0; i < cloud_names_dropdown.length; i++)
            {
                if(cloud_names_dropdown[i].selected == true)
                {
                    selected_dropdown_cloud_name = cloud_names_dropdown[i].value;
                    break;
                }
            }
        }
        org_id = domain_name.trim();
        domain_name = org_id + "." + selected_dropdown_cloud_name;
        if(fetch_domains(selected_dropdown_cloud_name, domain_name) == true)
        {
            selectMainMenuItem(selected_dropdown_cloud_name, domain_name);
        }
        else
        {
            alert("No data found for the provided organization ID.");
        }
        document.getElementById('search_field').value = org_id
    });
});